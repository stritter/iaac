datacenter = "dc-1"
data_dir = "$PWD/data"
ui = true

addresses {
    http = "0.0.0.0"
}

connect {
    enabled = true
}

ports {
    http = 8500
    https = 8501
    gRPC = 8502
}

server = true
bootstrap_expect = 3
retry_join = ["consul-server.consul.svc.cluster.local"]
