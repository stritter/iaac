#!/usr/bin/env bash

# create consul namespace
kubectl create ns consul

# create consul config map
kubectl create configmap consul-config \
    --from-file=config.hcl \
    --namespace consul

# create a serviceAccount and a clusterRoleBinding for Consul
kubectl create serviceaccount consul \
    --namespace consul
kubectl create clusterrolebinding consul-role \
    --clusterrole=consul \
    --serviceaccount=consul:consul \
    --namespace consul

# create a StatefulSet for Consul
kubectl create -f consul-statefulset.yaml --namespace consul

# create a service for Consul
kubectl create -f consul-service.yaml --namespace consul

# verify that Consul is running
kubectl get pods --namespace consul
